%global pypi_name jaraco.functools

Name:           python-jaraco-functools
Version:        2.0
Release:        1%{?dist}
Summary:        Functools like those found in stdlib
License:        MIT
URL:            https://github.com/jaraco/jaraco.functools
Source0:        https://files.pythonhosted.org/packages/source/j/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-jaraco-classes
BuildRequires:  python3-more-itertools
BuildRequires:  python3-pytest >= 3.5
BuildRequires:  python3-setuptools
BuildRequires:  python3-setuptools_scm >= 1.15.0
BuildRequires:  python3-six

%global _description\
Functools like those found in stdlib.

%description %_description

%package -n     python3-jaraco-functools
Summary:        %{summary}
%{?python_provide:%python_provide python3-jaraco-functools}
%description -n python3-jaraco-functools %_description

%prep
%autosetup -n %{pypi_name}-%{version}

# Remove unnecessary test deps
sed -i 's/ --doctest-modules//' pytest.ini
sed -i 's/ --flake8//' pytest.ini

%build
%py3_build

%install
%py3_install

%check
py.test-%{python3_version} -v

%files -n python3-jaraco-functools
%license LICENSE
%doc README.rst
%{python3_sitelib}/*

%changelog
* Mon Feb 25 2019 Ken Dreyer <kdreyer@redhat.com> - 2.0-1
- Initial package.
